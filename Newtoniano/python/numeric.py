import csv
import matplotlib.pyplot as plt
from numpy import arange

csvs = ["5ms.csv", "7.5ms.csv", "10ms.csv"]
u = [[],[],[]]
u_0 = ['5m/s', '7.5m/s', '10m/s']
time = arange(0,5,0.005)


def AppendU(arq, u):
    with open(arq, newline='') as file:
        reader = csv.reader(file, delimiter=',')
        next(reader, None) # pula o cabeçalho
        next(reader, None) # pula a primeira linha porque fiquei com preguiça de consertar
        for col in reader:
            u.append(col[2])

AppendU(csvs[0],u[0])
AppendU(csvs[1],u[1])
AppendU(csvs[2],u[2])

# print('Valor de u (5m/s): ', u[0], '\n')
# print('Valor de u (7.5m/s): ', u[1], '\n')
# print('Valor de u (10m/s): ', u[2], '\n')

plt.figure(figsize=(7,7))
plt.xlabel('Velocidade (m/s)')
plt.ylabel('Tempo (s)')

for u_n in u:
    count = 0
    print(u_n)
    plt.plot(time, u_n, label='Velocidade de %s:'%(u_0[count]))
    count += 1

plt.grid()
plt.legend()
# plt.show()
plt.savefig('Velocidades.png')


# Estado Newtoniano
# def Newtoniano:
